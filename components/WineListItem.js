/**
 * @providesModule components/WineListItem
 */
import React, {PropTypes} from 'react';
import {TouchableHighlight, StyleSheet, View, Text} from 'react-native';

const styles = StyleSheet.create({
  li: {
    backgroundColor: '#fff',
    borderBottomColor: '#eee',
    borderColor: 'transparent',
    borderWidth: 1,
    paddingLeft: 16,
    paddingTop: 14,
    paddingBottom: 16
  },
  liContainer: {
    flex: 2,
  },
  liText: {
    color: '#333',
    fontSize: 16,
  }
});

function WineListItem({ wine, onRemove, onPress }) {
  return (
      <TouchableHighlight onPress={onPress}>
      <View style={styles.li}>
        <Text style={styles.liText}>
          {wine.title}
        </Text>
        <Text>
          {wine.grape}
        </Text>
      </View>
      </TouchableHighlight>
    );
}

WineListItem.propTypes = {
  wine: PropTypes.object.isRequired,
  onRemove: PropTypes.func.isRequired,
  onPress: PropTypes.func.isRequired
};

export default WineListItem;
