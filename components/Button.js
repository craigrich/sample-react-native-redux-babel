/**
 * @providesModule components/Button
 */
import React, {PropTypes} from 'react';

import {StyleSheet, TouchableHighlight, Text} from 'react-native';

const buttonStyles = StyleSheet.create({
  primaryText: {
    fontSize: 16,
    color: 'white',
    alignSelf: 'center'
  },
  primary: {
    height: 45,
    backgroundColor: '#0c60ee',
    borderColor: '#0c60ee',
    borderWidth: 1,
    borderRadius: 2,
    marginBottom: 10,
    paddingLeft: 12,
    paddingRight: 12,
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
});

export default function Button({ onPress, children }) {
  return (
    <TouchableHighlight
      style={buttonStyles.primary}
      underlayColor="#e077de"
      onPress={onPress}
    >
      <Text style={buttonStyles.primaryText}>
        {children}
      </Text>
    </TouchableHighlight>
    );
}

Button.propTypes = {
  onPress: PropTypes.func.isRequired,
  children: PropTypes.string.isRequired
};
