/**
 * @providesModule components/WineListView
 */
import React, {PropTypes} from 'react';

import {StyleSheet, ListView, View, Text} from 'react-native';
import WineListItem from 'components/WineListItem';

const listStyles = StyleSheet.create({
  listview: {
    flex: 1,
  }
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

function WineListView({ wineList, onRemove, onPress }) {

  if (!wineList.length) {
    return (
      <View style={{ alignItems: 'center' }}>
        <Text>
          Get started by adding your first wine
        </Text>
      </View>
    );
  }

  return (
    <ListView
      dataSource={ds.cloneWithRows(wineList)}
      style={listStyles.listView}
      renderRow={(wineData) => (
        <WineListItem
          wine={wineData}
          onRemove={onRemove}
          onPress={() => onPress(wineData)}
        />
      )}
    />
    );
}

WineListView.propTypes = {
  wineList: PropTypes.array.isRequired,
  onRemove: PropTypes.func.isRequired,
  onPress: PropTypes.func.isRequired
};

export default WineListView;
