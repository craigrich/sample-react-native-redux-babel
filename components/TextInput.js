/**
 * @providesModule components/TextInput
 */
import React, {Component, PropTypes} from 'react';

import {StyleSheet, View, TextInput} from 'react-native';

const styles = StyleSheet.create({
  inputContainer: {
    borderColor: '#DDD',
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    marginBottom: 20
  },
  input: {
    fontSize: 16,
    height: 40
  }
});


export default class TipsyTextInput extends Component {

  componentDidMount() {
    this.setState({
      value: this.props.value
    });
  }

  handleChange(value) {
    this.setState({ value });
    this.props.onChange({
      [this.props.name]: value
    });
  }

  render() {
    return (
      <View style={styles.inputContainer}>
        <TextInput
          ref={component => {this._textInput = component;}}
          value={this.state ? this.state.value : ''}
          style={styles.input}
          placeholder={this.props.placeholder}
          onChangeText={text => this.handleChange(text)}
        />
      </View>
      );
  }
}

TipsyTextInput.propTypes = {
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string.isRequired
};
