import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import App from 'containers/App';
import store from 'redux/store';

function Tipsy() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
    );
}

AppRegistry.registerComponent('Tipsy', () => Tipsy);
