/**
 * @providesModule containers/WineList
 */
import React, {PropTypes} from 'react';

import {View} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as WineListActions from 'redux/modules/wineList';
import * as WineActions from 'redux/modules/wine';

import WineListView from 'components/WineListView';
import styles from 'styles';

function WineList({ wineList, removeWine, loadWine }) {
  return (
    <View style={styles.container}>
      <WineListView
        wineList={wineList}
        onRemove={removeWine}
        onPress={wine => loadWine(wine)}
      />
    </View>
    );
}

WineList.propTypes = {
  wineList: PropTypes.array.isRequired,
  removeWine: PropTypes.func.isRequired,
  loadWine: PropTypes.func.isRequired,
  navigator: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { wineList: state.wineList };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...WineListActions, ...WineActions }, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WineList);
