/**
 * @providesModule containers/App
 */
import React, {Component, PropTypes} from 'react';
import {NavigatorIOS} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as RouterActions from 'redux/modules/router';

class App extends Component {

  componentDidMount() {
    this.props.initRouter(this.refs.nav);
  }

  render() {
    return (
      <NavigatorIOS
        style={{ flex: 1 }}
        ref="nav"
        initialRoute={this.props.currentRoute}
      />
      );
  }
}

App.propTypes = {
  initRouter: PropTypes.func.isRequired,
  currentRoute: PropTypes.object
};

function mapStateToProps(state) {
  return { currentRoute: state.router };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(RouterActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
