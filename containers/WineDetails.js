/**
 * @providesModule containers/WineDetails
 */
import React, {PropTypes} from 'react';

import {View, Text} from 'react-native';
import { connect } from 'react-redux';
import styles from 'styles';

function WineList({ wine }) {
  return (
    <View style={styles.container}>
      <Text>
        Wine Details Page
      </Text>
      <Text>
        {wine.title}
      </Text>
      <Text>
        {wine.grape}
      </Text>
    </View>
    );
}

WineList.propTypes = {
  wine: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { wine: state.wine };
}

function mapDispatchToProps() {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WineList);
