/**
 * @providesModule containers/WineAdd
 */
import React, {PropTypes} from 'react';
import {StyleSheet, View} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as WineActions from 'redux/modules/wine';

import TipsyTextInput from 'components/TextInput';
import Button from 'components/Button';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    paddingTop: 80,
    padding: 10
  }
});

function WineAdd({ wine, editWine, createWine }) {
  return (
    <View style={styles.container}>
      <TipsyTextInput
        name="title"
        label="Wine Title"
        value={wine.title}
        onChange={editWine}
        placeholder="Wine Name"
      />
      <TipsyTextInput
        name="grape"
        label="Grape"
        value={wine.grape}
        onChange={editWine}
        placeholder="Grape"
      />
      <Button onPress={createWine}>
        Add Wine
      </Button>
    </View>
    );
}

WineAdd.propTypes = {
  wine: PropTypes.object.isRequired,
  editWine: PropTypes.func.isRequired,
  createWine: PropTypes.func.isRequired,
  navigator: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return { wine: state.wine };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(WineActions, dispatch);
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WineAdd);
