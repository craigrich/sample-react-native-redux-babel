/**
 * @providesModule redux/modules/wineList
 */
const WINELIST_PUSH = 'tipsy/wineList/PUSH';
const WINELIST_PUT = 'tipsy/wineList/ADD';
const WINELIST_DELETE = 'tipsy/wineList/DELETE';

export default function wineListReducer(state = [], action = {}) {
  switch (action.type) {
    case WINELIST_PUSH:
      return state.concat(action.data);
    case WINELIST_PUT:
      return state.map(wineListItem => {
        if (wineListItem.id === action.data.id) {
          return action.data;
        }
        return wineListItem;
      }
    case WINELIST_DELETE:
      return state.filter(item => {
        return item.title !== action.data.title);
      }
    default:
      return state;
  }
}

/**
 * Adds new wine to list
 * @param {Object} wineData
 * @return {Object}
 */
export function addToWineList(wineData) {
  return {
    type: WINELIST_PUSH,
    data: wineData
  };
}

/**
 * Updates an existing wine in the list
 * @param  {Object} wineData
 * @return {Object}
 */
export function updateWineList(wineData) {
  return {
    type: WINELIST_PUT,
    data: wineData
  };
}

/**
 * Removes a wine from the list
 * @param  {Object} wineData
 * @return {Object}
 */
export function removeFromWineList(wineData) {
  return {
    type: WINELIST_DELETE,
    data: wineData
  };
}
