/**
 * @providesModule redux/modules/reducer
 */
import { combineReducers } from 'redux';
import wineListReducer from './wineList';
import wineReducer from './wine';
import routerReducer from './router';

/**
 * Combines all reducers to be injected into a Redux store
 */
export default combineReducers({
  wine: wineReducer,
  wineList: wineListReducer,
  router: routerReducer || {}
});
