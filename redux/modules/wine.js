/**
 * @providesModule redux/modules/wine
 */
import shortid from 'shortid';
import { addToWineList, updateWineList } from 'redux/modules/wineList';
import { pop, push } from 'redux/modules/router';

const WINE_GET = 'tipsy/wine/GET';
const WINE_PUT = 'tipsy/wine/PUT';
const WINE_UPDATE_INPUTS = 'tipsy/wine/UPDATE_INPUTS';
const WINE_CREATE = 'tipsy/wine/CREATE';

export default function wineReducer(state = {}, action = {}) {
  switch (action.type) {
    case WINE_GET:
      return { ...state, ...action.data };
    case WINE_UPDATE_INPUTS:
      return Object.assign(state, action.data);
    case WINE_PUT:
      return state;
    case WINE_CREATE:
      return state;
    default:
      return state;
  }
}

/**
 * Loads selected wine data and transition
 * to wine details route
 * @param  {Object} wineData
 * @return {Object}
 */
export function loadWine(wineData) {
  return (dispatch) => {
    dispatch({
      type: WINE_GET,
      data: wineData
    });
    dispatch(push('wineDetails', wineData.title));
  };
}

/**
 * Saves Wine info from native form Inputs
 * @param  {Object} wineData 
 * @return {Object}
 */
export function editWine(wineData) {
  return {
    type: WINE_UPDATE_INPUTS,
    data: wineData
  };
}

/**
 * Saves Wine, updates winelist and 
 * pops the current route.
 * @return {Object}
 */
export function saveWine() {
  return (dispatch, getState) => {
    dispatch({ type: WINE_PUT });
    dispatch(updateWineList(getState().wine));
    dispatch(pop());
  };
}

/**
 * Adds Wine, adds to winelist and 
 * pops the current route.
 * @return {Object}
 */
export function createWine() {
  return (dispatch, getState) => {
    dispatch({ type: WINE_CREATE });
    dispatch(addToWineList({
      ...getState().wine,
      id: shortid.generate()
    }));
    dispatch(pop());
  };
}
