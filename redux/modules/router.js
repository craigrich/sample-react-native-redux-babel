/**
 * @providesModule redux/modules/router
 */
import WineList from 'containers/WineList';
import WineAdd from 'containers/WineAdd';
import WineDetails from 'containers/WineDetails';
import WineEdit from 'containers/WineEdit';

let store = {};

const ROUTER_PUSH = 'tipsy/router/PUSH';
const ROUTER_POP = 'tipsy/router/POP';
const ROUTER_INIT = 'tipsy/router/INIT';

/**
 * State config for IOS Router
 * @type {Object}
 */
const routes = {
  wineAdd: {
    title: 'Add Wine',
    component: WineAdd
  },
  wineList: {
    title: 'Tipsy',
    component: WineList,
    rightButtonTitle: 'Add Wine',
    onRightButtonPress: () => store.dispatch(push('wineAdd'))
  },
  wineDetails: {
    title: 'Wine Details',
    component: WineDetails,
    rightButtonTitle: 'Edit',
    onRightButtonPress: () => store.dispatch(push('wineEdit'))
  },
  wineEdit: {
    title: 'Edit your Wine',
    component: WineEdit,
    leftButtonTitle: 'Back',
    onLeftButtonPress: () => store.dispatch(pop())
  }
};

export default function routerReducer(state = routes.wineList, action = {}) {
  switch (action.type) {
    case ROUTER_INIT:
      return action.data;
    case ROUTER_PUSH:
      state.push(action.data);
      return state;
    case ROUTER_POP:
      state.pop();
      return state;
    default:
      return state;
  }
}

/**
 * Push and transition to new route onto Navigator
 * @param  {String} routeName The given route name 
 * @return {Object}
 */
export function push(routeName) {
  const route = { ...routes[routeName] };
  return {
    type: ROUTER_PUSH,
    data: route
  };
}

/**
 * pops current route off the Navigator
 * @return {Object}
 */
export function pop() {
  return {
    type: ROUTER_POP
  };
}

/**
 * Binds IOS Navigator to state.
 * @param  {Object} navigator IOS Navigator
 * @return {Object} 
 */
export function initRouter(navigator) {
  store = require('redux/store');
  return {
    type: ROUTER_INIT,
    data: navigator
  };
}
