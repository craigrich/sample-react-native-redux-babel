/**
 * @providesModule redux/store
 */
import { Platform } from 'react-native';
import { createStore as _createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from 'redux/modules/reducer';
import Immutable from 'immutable';


/**
 * Prepares Redux Middleware
 * @param {bool} isDevelopment is React Native in dev mode
 */
function createEnhancer(isDevelopment) {

  const middlewares = [thunk];

  if (isDevelopment) {
    require('immutable-devtools')(Immutable);
    return compose(
      applyMiddleware(...middlewares),
      require('remote-redux-devtools')({
        name: Platform.OS,
        hostname: 'localhost',
        port: 5678
      })
    );
  }

  return applyMiddleware(...middlewares);

}

/**
 * compose Redux store from our reducers,
 * middleware and initial state
 * @param  {Object} initialState
 * @return {Object}              Our Redux Store
 */
function createStore(initialState) {
  const enhancer = createEnhancer(global.__DEV__);
  const store = _createStore(rootReducer, initialState, enhancer);
  return store;
}

export default createStore();
