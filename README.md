A few RN samples from a wine journal app I'm working on.

I've taken a few hints from the [Redux Hot boilerplate](https://github.com/erikras/react-redux-universal-hot-example) along with using this [Redux layout proposal](https://github.com/erikras/ducks-modular-redux).